import { Injectable } from '@angular/core';
import { Recipe } from './Classes/recipe.class';
import { Ingredient } from './Classes/ingredient.class';
import { Fridge } from './Classes/fridge.class';


@Injectable({
  providedIn: 'root'
})
export class RecipeMgmtService {

  shoppingList: Array<Ingredient> = [];
  recipeItemsInFridge: Array<Ingredient> = [];
  recipeList: Array<Recipe> = [];
  fridgeContents: Array<Ingredient> = [];
  selectedRecipe: Recipe = null;

  constructor() { }

  getFridgeContents() {
    return this.fridgeContents;
  }

  getRecipeList() {
    return this.recipeList;
  }

  getShoppingList() {
    return this.shoppingList;
  }

  getRecipeItemsInFridge() {
    return this.recipeItemsInFridge;
  }

  checkRecipe(recipe: Recipe) {
    this.recipeItemsInFridge = [];
    this.shoppingList = [];
    recipe.items.forEach(element => {
        let isInFridge = false;
        let amtNeeded;
        this.fridgeContents.forEach(foodInFridge => {
            if (element.name === foodInFridge.name) {
                isInFridge = true;
                this.recipeItemsInFridge.push(foodInFridge);
                if (element.quantity > foodInFridge.quantity) {
                    amtNeeded = element.quantity - foodInFridge.quantity;
                    this.shoppingList.push(new Ingredient(element.name, amtNeeded));
                }
            }
        });
        if (!isInFridge) {
            amtNeeded = element.quantity;
            this.shoppingList.push(element);
        }
    });
  }

}
