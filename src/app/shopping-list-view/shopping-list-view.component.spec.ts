import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShoppingListViewComponent } from './shopping-list-view.component';
import { FormsModule } from '@angular/forms';
import { RecipeMgmtService } from '../recipe-mgmt.service';


describe('ShoppingListViewComponent', () => {
  let component: ShoppingListViewComponent;
  let fixture: ComponentFixture<ShoppingListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoppingListViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
