import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../Classes/ingredient.class';
import { Recipe } from '../Classes/recipe.class';
import { Fridge } from '../Classes/fridge.class';
import { RecipeMgmtService } from '../recipe-mgmt.service';

@Component({
  selector: 'app-shopping-list-view',
  templateUrl: './shopping-list-view.component.html',
  styleUrls: ['./shopping-list-view.component.css']
})
export class ShoppingListViewComponent implements OnInit {

  shoppingList: Array<Ingredient> = [];
  recipeItemsInFridge: Array<Ingredient> = [];
  message: string = null;
  hasCheckedIngredients = false;

  constructor(private recipeMgmtService: RecipeMgmtService) { }

  ngOnInit() {
  }

  getLists() {
    if (this.recipeMgmtService.selectedRecipe != null) {
      this.recipeMgmtService.checkRecipe(this.recipeMgmtService.selectedRecipe);
      this.shoppingList = this.recipeMgmtService.getShoppingList();
      this.recipeItemsInFridge = this.recipeMgmtService.getRecipeItemsInFridge();
      this.message = null;
      this.hasCheckedIngredients = true;
    } else {
      this.message = 'Select a recipe to generate your shopping list.';
      this.shoppingList = [];
      this.recipeItemsInFridge = [];
      this.hasCheckedIngredients = false;
    }
  }

}
