import { TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RecipeViewComponent } from './recipe-view/recipe-view.component';
import { FridgeViewComponent } from './fridge-view/fridge-view.component';
import { ShoppingListViewComponent } from './shopping-list-view/shopping-list-view.component';
import { RecipeMgmtService } from './recipe-mgmt.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, RecipeViewComponent, FridgeViewComponent, ShoppingListViewComponent
      ],
      imports: [ FormsModule],
      providers: [RecipeMgmtService]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
