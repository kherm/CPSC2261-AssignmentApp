import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RecipeViewComponent } from './recipe-view/recipe-view.component';
import { FridgeViewComponent } from './fridge-view/fridge-view.component';
import { ShoppingListViewComponent } from './shopping-list-view/shopping-list-view.component';
import { RecipeMgmtService } from './recipe-mgmt.service';

@NgModule({
  declarations: [
    AppComponent,
    RecipeViewComponent,
    FridgeViewComponent,
    ShoppingListViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [RecipeMgmtService],
  bootstrap: [AppComponent]
})
export class AppModule { }
