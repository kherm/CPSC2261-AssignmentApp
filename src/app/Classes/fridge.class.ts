import { Recipe } from './recipe.class';
import { Ingredient } from './ingredient.class';

export class Fridge {

    contents: Array<Ingredient> = [];

    constructor() {}

    add(item: string, amount: number) {
        let isInFridge = false;
        this.contents.forEach(element => {
            if (element.name === item) {
                element.quantity += amount;
                isInFridge = true;
            }
        });
        if (!isInFridge) {
            this.contents.push(new Ingredient(item, amount));
        }
    }

    remove(item: string, amount: number) {
        this.contents.forEach(element => {
            if (element.name === item) {
                element.quantity -= amount;
                if (element.quantity <= 0) {
                    const index = this.contents.indexOf(element);
                    this.contents.splice(index, 1);
                }
            }
        });
    }

}
