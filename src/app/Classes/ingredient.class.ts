export class Ingredient {

    constructor(public name: string, public quantity: number) {}

    add() {
        this.quantity += 2;
    }

    subtract() {
        if (this.quantity >= 2 ) {
            this.quantity -= 2;
        } else {
            this.quantity = 0;
        }
    }
}
