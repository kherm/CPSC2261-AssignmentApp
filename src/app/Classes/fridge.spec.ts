import { Recipe } from './recipe.class';
import { Ingredient } from './ingredient.class';
import { Fridge } from './fridge.class';

describe('FridgeSuite', function() {
    const fridge = new Fridge();

    it('addItem', function() {
        fridge.add('chickpeas', 1);
        fridge.add('carrots', 2);
        fridge.add('potatoes', 3);
        fridge.add('greenBeans', 4);
        fridge.add('carrots', 2);
        expect(fridge.contents.length).toBe(4);
        expect(fridge.contents[1].quantity).toBe(4);
    });

    it('removeItem', function() {
        fridge.remove('chickpeas', 1);
        fridge.remove('carrots', 5);
        fridge.remove('potatoes', 1);
        fridge.remove('onions', 2);
        expect(fridge.contents.length).toBe(2);
        expect(fridge.contents[0].quantity).toBe(2);
    });

    // it('checkRecipeInFridge', function() {
    //     fridge.add('peas', 200);
    //     fridge.add('carrots', 1);
    //     console.log(fridge.contents);

    //     const stew = new Recipe('stew', 20);
    //     stew.addItem(new Ingredient('peas', 100));
    //     stew.addItem(new Ingredient('carrots', 2));
    //     stew.addItem(new Ingredient('beef', 1));
    //     stew.addItem(new Ingredient('potatoes', 2));

    //     const foodList = fridge.checkRecipe(stew);
    //     // # of shopping list items
    //     expect(foodList[0].length).toBe(2);
    //     // shopping list should contain 1 carrot
    //     expect(foodList[0]).toContain(jasmine.objectContaining({name: 'carrots', quantity: 1}));
    //     // shopping list should contain 1 beef
    //     expect(foodList[0]).toContain(jasmine.objectContaining({name: 'beef', quantity: 1}));
    //     // # of recipe items in fridge
    //     expect(foodList[1].length).toBe(3);
    //     expect(foodList[1][0].quantity).toBe(200);    // peas in fridge
    //     expect(foodList[1]).not.toContain({name: 'beef'});  // beef should not be in fridge
    //     expect(foodList[1]).toContain(jasmine.objectContaining({name: 'potatoes'}));
    // });

});
