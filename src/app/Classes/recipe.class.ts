import { Ingredient } from './ingredient.class';

export class Recipe {

    items: Array<Ingredient> = [];
    steps: Array<string> = [];

    constructor(public name: string, public estTime: number) {}

    addItem(item: Ingredient) {
      item.name = item.name.toLowerCase();
      let itemFound = false;

      this.items.forEach(element => {
        if (item.name === element.name) {
          element.quantity += item.quantity;
          itemFound = true;
        }
      });
      if (!itemFound) {
        this.items.push(item);
      }
    }

    addStep(step: string) {
        this.steps.push(step);
    }
}
