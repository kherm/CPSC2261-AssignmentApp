import { Recipe } from './recipe.class';
import { Ingredient } from './ingredient.class';

describe('RecipeSuite', function() {

    const chickpeas = new Ingredient('chickpeas', 10);
    const carrots = new Ingredient('carrots', 2);
    const potatoes = new Ingredient('potatoes', 1);
    const greenBeans = new Ingredient('greenBeans', 1);
    const carrots2 = new Ingredient('carrots', 2);

    const gooLash = new Recipe('gooLash', 20);

    it('addItem', function() {
        gooLash.addItem(chickpeas);
        gooLash.addItem(carrots);
        gooLash.addItem(potatoes);
        gooLash.addItem(greenBeans);
        gooLash.addItem(carrots2);
        expect(gooLash.items.length).toBe(4);
        expect(gooLash.items[3].quantity).toBe(1);
        expect(gooLash.items[1].quantity).toBe(4);
    });

    it('addStep', function() {
        gooLash.addStep('Chop up all ingredients');
        gooLash.addStep('Fry it up on medium heat with a bit of oil');
        gooLash.addStep('Add a sauce of your choice');
        gooLash.addStep('Simmer for 10 minutes');
        expect(gooLash.steps.length).toBe(4);
        expect(gooLash.steps[0]).toBe('Chop up all ingredients');
    });

});
