import { Ingredient } from './ingredient.class';

describe('IngredientSuite', function() {

    let chickpeas = new Ingredient('chickpeas', 10);
    let carrots = new Ingredient('carrots', 2);
    let potatoes = new Ingredient('potatoes', 0);
    let greenBeans = new Ingredient('greenBeans', 1);

    it('subtract', function() {
        chickpeas.subtract();
        carrots.subtract();
        potatoes.subtract();
        greenBeans.subtract();

        expect(chickpeas.quantity).toBe(8);
        expect(carrots.quantity).toBe(0);
        expect(potatoes.quantity).toBe(0);
        expect(greenBeans.quantity).toBe(0);
    });

    it('add', function() {
        carrots.add();
        carrots.add();
        expect(carrots.quantity).toBe(4);
        expect(carrots.name).toBe('carrots');
    });

});
