import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FridgeViewComponent } from './fridge-view.component';
import { FormsModule } from '@angular/forms';
import { Fridge } from '../Classes/fridge.class';
import { RecipeMgmtService } from '../recipe-mgmt.service';

describe('FridgeViewComponent', () => {
  let component: FridgeViewComponent;
  let fixture: ComponentFixture<FridgeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FridgeViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FridgeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
