import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../Classes/ingredient.class';
import { Recipe } from '../Classes/recipe.class';
import { Fridge } from '../Classes/fridge.class';
import { RecipeMgmtService } from '../recipe-mgmt.service';

@Component({
  selector: 'app-fridge-view',
  templateUrl: './fridge-view.component.html',
  styleUrls: ['./fridge-view.component.css']
})
export class FridgeViewComponent implements OnInit {

  fridge: Fridge = new Fridge();
  itemBinding = '';
  quantityBinding = 0;
  selectedItem: Ingredient = null;
  message: string = null;

  constructor(private recipeMgmtService: RecipeMgmtService) { }

  ngOnInit() {
  }

  addItem() {
    if (this.itemBinding !== '') {
      this.fridge.add(this.itemBinding, this.quantityBinding);
      this.recipeMgmtService.fridgeContents = this.fridge.contents;
    } else {
      this.message = 'Enter an item and its quantity to add to the fridge.';
    }
  }

  select(choice) {
    if (this.selectedItem === choice) {
      this.selectedItem = null;
    } else {
      this.selectedItem = choice;
      this.itemBinding = choice.name;
      this.quantityBinding = choice.quantity;
    }
    this.message = null;
  }

  deleteItem() {
    if (this.selectedItem != null) {
      this.fridge.remove(this.itemBinding, this.quantityBinding);
      this.recipeMgmtService.fridgeContents = this.fridge.contents;
      this.selectedItem = null;
    } else {
      this.message = 'Select an item and enter the quantity to remove.';
    }
  }

  // getLists() {
  //   const recipe = this.recipeMgmtService.selectedRecipe;
  //   if ( recipe != null) {
  //     this.recipeMgmtService.bothLists = this.fridge.checkRecipe(recipe);
  //   }
  // }
}
