import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../Classes/ingredient.class';
import { Recipe } from '../Classes/recipe.class';
import { Fridge } from '../Classes/fridge.class';
import { RecipeMgmtService } from '../recipe-mgmt.service';

@Component({
  selector: 'app-recipe-view',
  templateUrl: './recipe-view.component.html',
  styleUrls: ['./recipe-view.component.css']
})
export class RecipeViewComponent implements OnInit {
  recipeBox: Array<Recipe> = [];
  recipeBinding: Recipe = new Recipe('', 0);
  ingredientBinding: Ingredient = new Ingredient('', 0);
  stepBinding: string = null;
  selectedItem: Recipe = null;
  message: string = null;
  showEditButtons = false;

  constructor(private recipeMgmtService: RecipeMgmtService) { }

  ngOnInit() {
  }

  addRecipe() {
    if (!this.showEditButtons) {
      this.recipeBox.push(new Recipe(this.recipeBinding.name, this.recipeBinding.estTime));
      this.recipeMgmtService.recipeList = this.recipeBox;
    } else {
      this.message = 'Click Done before trying to add a new recipe.';
    }
  }

  select(choice) {
    if (this.showEditButtons) {
      this.message = 'Click Done before selecting another recipe.';
    } else {
      if (this.selectedItem === choice) {
        this.selectedItem = null;
        this.recipeMgmtService.selectedRecipe = null;
      } else {
        this.selectedItem = choice;
        this.recipeMgmtService.selectedRecipe = choice;
      }
      this.message = null;
    }
  }

  deleteRecipe() {
    if (this.selectedItem != null && !this.showEditButtons) {
      const index = this.recipeBox.indexOf(this.selectedItem);
      this.recipeBox.splice(index, 1);
      this.recipeMgmtService.recipeList = this.recipeBox;
      this.selectedItem = null;
    } else {
      if (this.selectedItem == null) {
        this.message = 'Select a recipe to delete.';
      } else {
        this.message = 'Click Done before trying to delete a recipe.';
      }
    }
  }

  editRecipe() {
    if (this.selectedItem != null) {
      this.showEditButtons = true;
    } else {
      this.message = 'Select a recipe to edit.';
    }
  }

  done() {
    this.showEditButtons = false;
  }

  addIngredient() {
    const index = this.recipeBox.indexOf(this.selectedItem);
    this.recipeBox[index].addItem(new Ingredient(this.ingredientBinding.name, this.ingredientBinding.quantity));
    this.recipeMgmtService.recipeList[index] = this.recipeBox[index];
  }

  addStep() {
    const index = this.recipeBox.indexOf(this.selectedItem);
    this.recipeBox[index].addStep(this.stepBinding);
    this.recipeMgmtService.recipeList[index] = this.recipeBox[index];
  }
}
