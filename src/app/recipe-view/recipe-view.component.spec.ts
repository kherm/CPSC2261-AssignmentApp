import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RecipeViewComponent } from './recipe-view.component';
import { FormsModule } from '@angular/forms';
import { Recipe } from '../Classes/recipe.class';
import { RecipeMgmtService } from '../recipe-mgmt.service';


describe('RecipeViewComponent - Empty Box', () => {
  let component: RecipeViewComponent;
  let fixture: ComponentFixture<RecipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should have message "Your recipe box is empty"', async(() => {
    const elem: HTMLElement = fixture.nativeElement;
    expect(elem.querySelector('td#empty').textContent).toBe('Your recipe box is empty!');
  }));
});



describe('RecipeViewComponent - Methods', () => {
  let component: RecipeViewComponent;
  let fixture: ComponentFixture<RecipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    this.recipeService = new RecipeMgmtService();
    this.recipeApp = new RecipeViewComponent(this.recipeService);
  });

  it('addRecipe()', async(() => {
    this.recipeApp.addRecipe();
    expect(this.recipeApp.recipeBox.length).toBe(1);

    this.recipeApp.showEditButtons = true;
    this.recipeApp.addRecipe();
    expect(this.recipeApp.message).toBe('Click Done before trying to add a new recipe.');
  }));

  it('select(choice)', async(() => {
    const recipeSelect = new Recipe('stew', 10);

    this.recipeApp.showEditButtons = true;
    this.recipeApp.select(recipeSelect);
    expect(this.recipeApp.message).toBe('Click Done before selecting another recipe.');

    this.recipeApp.showEditButtons = false;
    this.recipeApp.selectedItem = recipeSelect;
    this.recipeApp.select(recipeSelect);
    expect(this.recipeApp.selectedItem).toBe(null);
    expect(this.recipeApp.message).toBe(null);

    this.recipeApp.selectedItem = null;
    this.recipeApp.select(recipeSelect);
    expect(this.recipeApp.selectedItem).toBe(recipeSelect);
  }));

  it('deleteRecipe()', async(() => {
    this.recipeApp.addRecipe();

    this.recipeApp.deleteRecipe();
    expect(this.recipeApp.message).toBe('Select a recipe to delete.');

    this.recipeApp.selectedItem = new Recipe('stew', 10);
    this.recipeApp.showEditButtons = true;

    this.recipeApp.deleteRecipe();
    expect(this.recipeApp.message).toBe('Click Done before trying to delete a recipe.');

    this.recipeApp.showEditButtons = false;
    this.recipeApp.deleteRecipe();
    expect(this.recipeApp.recipeBox.length).toBe(0);
  }));

  it('editRecipe()', async(() => {
    this.recipeApp.editRecipe();
    expect(this.recipeApp.message).toBe('Select a recipe to edit.');
    expect(this.recipeApp.showEditButtons).toBe(false);

    this.recipeApp.selectedItem = new Recipe('stew', 10);
    this.recipeApp.editRecipe();
    expect(this.recipeApp.showEditButtons).toBe(true);
  }));

  it('done()', async(() => {
    expect(this.recipeApp.showEditButtons).toBe(false);
    this.recipeApp.showEditButtons = true;
    this.recipeApp.done();
    expect(this.recipeApp.showEditButtons).toBe(false);
  }));

  it('addIngredient()', async(() => {
    this.recipeApp.selectedItem = new Recipe('stew', 10);
    this.recipeApp.recipeBox.push(this.recipeApp.selectedItem);
    this.recipeApp.addIngredient();
    expect(this.recipeApp.recipeBox[0].items.length).toBe(1);
  }));

  it('addStep()', async(() => {
    this.recipeApp.selectedItem = new Recipe('stew', 10);
    this.recipeApp.recipeBox.push(this.recipeApp.selectedItem);
    this.recipeApp.addStep();
    this.recipeApp.addStep();
    expect(this.recipeApp.recipeBox[0].steps.length).toBe(2);
  }));
});





describe('RecipeViewComponent - Add Recipe to Table', () => {
  let component: RecipeViewComponent;
  let fixture: ComponentFixture<RecipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('manually drive inputs', () => {
    const elem: HTMLElement = fixture.nativeElement;

    const inputRecipe = elem.querySelectorAll('input.inputRecipe');
    expect(inputRecipe.length).toBe(2);
    (<HTMLInputElement>(inputRecipe[0])).value = 'Stew';
    (<HTMLInputElement>(inputRecipe[1])).value = '10';

    const inputRecipeButton = elem.querySelector('input.inputRecipeButton');
    inputRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(elem.querySelector('td#empty')).toBeFalsy();
    let outputTableName = elem.querySelector('td.recipeName');
    let outputTableTime = elem.querySelector('td.timeCol');
    (<HTMLInputElement>outputTableName).value = 'Stew';
    (<HTMLInputElement>outputTableTime).value = '10';

    (<HTMLInputElement>(inputRecipe[0])).value = 'Curry';
    (<HTMLInputElement>(inputRecipe[1])).value = '20:30';

    inputRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    outputTableName = elem.querySelector('td.recipeName');
    outputTableTime = elem.querySelector('td.timeCol');
    (<HTMLInputElement>outputTableName).value = 'Curry';
    (<HTMLInputElement>outputTableTime).value = '20:30';
  });
});

describe('RecipeViewComponent - Highlight Recipe', () => {
  let component: RecipeViewComponent;
  let fixture: ComponentFixture<RecipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    this.elem = HTMLElement = fixture.nativeElement;
    this.inputRecipe = this.elem.querySelectorAll('input.inputRecipe');

    (<HTMLInputElement>(this.inputRecipe[0])).value = 'Stew';
    (<HTMLInputElement>(this.inputRecipe[1])).value = '10';

    this.inputRecipeButton = this.elem.querySelector('input.inputRecipeButton');
    this.inputRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();
  });

  it('should highlight receipe when selected', () => {
    const recipeSelect = this.elem.querySelector('tr.list');
    (<HTMLInputElement>recipeSelect).click();
    fixture.detectChanges();
    expect(this.elem.querySelector('.selected')).toBeTruthy();
  });

  it('should unhighlight receipe when unselected', () => {
    const recipeSelect = this.elem.querySelector('tr.list');
    (<HTMLInputElement>recipeSelect).click();
    (<HTMLInputElement>recipeSelect).click();
    fixture.detectChanges();
    expect(this.elem.querySelector('.selected')).toBeFalsy();
  });
});

describe('RecipeViewComponent - Show Recipe Details', () => {
  let component: RecipeViewComponent;
  let fixture: ComponentFixture<RecipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    this.elem = HTMLElement = fixture.nativeElement;
    this.inputRecipe = this.elem.querySelectorAll('input.inputRecipe');

    (<HTMLInputElement>(this.inputRecipe[0])).value = 'Stew';
    (<HTMLInputElement>(this.inputRecipe[1])).value = '10';

    this.inputRecipeButton = this.elem.querySelector('input.inputRecipeButton');
    this.inputRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();
  });

  it('manually drive inputs', () => {
    const editRecipeButton = this.elem.querySelector('input.editRecipeButton');
    const recipeSelect = this.elem.querySelector('tr.list');

    (<HTMLInputElement>recipeSelect).click();
    editRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(this.elem.querySelector('.recipeDetails')).toBeTruthy();
  });
});

describe('RecipeViewComponent - Edit Recipe', () => {
  let component: RecipeViewComponent;
  let fixture: ComponentFixture<RecipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    this.elem = HTMLElement = fixture.nativeElement;
    this.inputRecipe = this.elem.querySelectorAll('input.inputRecipe');

    (<HTMLInputElement>(this.inputRecipe[0])).value = 'Stew';
    (<HTMLInputElement>(this.inputRecipe[1])).value = '10';

    this.inputRecipeButton = this.elem.querySelector('input.inputRecipeButton');
    this.inputRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    this.editRecipeButton = this.elem.querySelector('input.editRecipeButton');
    this.recipeSelect = this.elem.querySelector('tr.list');

    (<HTMLInputElement>this.recipeSelect).click();
    this.editRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();
  });

  it('manually drive inputs - add ingredients', () => {
    const inputIngredient = this.elem.querySelectorAll('input.inputIngredient');

    (<HTMLInputElement>(inputIngredient[0])).value = 'Beef';
    (<HTMLInputElement>(inputIngredient[1])).value = '1';
    expect(inputIngredient.length).toBe(2);

    const addIngredientButton = this.elem.querySelector('input.addIngredientButton');
    addIngredientButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(this.elem.querySelector('li')).toBeTruthy();
  });

  it('manually drive inputs - add steps', () => {
    const inputStep = this.elem.querySelectorAll('input.inputStep');

    (<HTMLInputElement>(inputStep[0])).value = 'Boil water';
    expect(inputStep.length).toBe(1);

    const addStepButton = this.elem.querySelector('input.addStepButton');
    addStepButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(this.elem.querySelector('li')).toBeTruthy();
  });
});

describe('RecipeViewComponent - Delete Recipe from Table', () => {
  let component: RecipeViewComponent;
  let fixture: ComponentFixture<RecipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeViewComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    this.elem = HTMLElement = fixture.nativeElement;
    this.inputRecipe = this.elem.querySelectorAll('input.inputRecipe');

    (<HTMLInputElement>(this.inputRecipe[0])).value = 'Stew';
    (<HTMLInputElement>(this.inputRecipe[1])).value = '10';

    this.inputRecipeButton = this.elem.querySelector('input.inputRecipeButton');
    this.inputRecipeButton.dispatchEvent(new Event('click'));
    this.inputRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    this.recipeSelect = this.elem.querySelector('tr.list');
    (<HTMLInputElement>this.recipeSelect).click();
    fixture.detectChanges();
  });

  it('manually drive inputs - should remove items from table when selected', () => {
    const deleteRecipeButton = this.elem.querySelector('input.deleteRecipeButton');
    deleteRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(this.elem.querySelector('tr.list')).toBeTruthy();

    this.recipeSelect = this.elem.querySelector('tr.list');
    (<HTMLInputElement>this.recipeSelect).click();
    deleteRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(this.elem.querySelector('tr.list')).toBeFalsy();
  });

  it('manually drive inputs - should  not remove items from table when not selected', () => {
    const deleteRecipeButton = this.elem.querySelector('input.deleteRecipeButton');
    (<HTMLInputElement>this.recipeSelect).click();
    deleteRecipeButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(this.elem.querySelector('tr.list')).toBeTruthy();
  });
});
