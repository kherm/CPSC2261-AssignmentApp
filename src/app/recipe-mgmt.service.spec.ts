import { TestBed, inject } from '@angular/core/testing';

import { RecipeMgmtService } from './recipe-mgmt.service';

describe('RecipeMgmtService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipeMgmtService]
    });
  });

  it('should be created', inject([RecipeMgmtService], (service: RecipeMgmtService) => {
    expect(service).toBeTruthy();
  }));
});
